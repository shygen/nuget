﻿namespace Shygen.Editor.Test.Generation.Attributes
{
    using System;
    using System.ComponentModel;
    using Runtime.Attributes;

    public class ModelCategoryAttribute : CategoryAtribute
    {
        public ModelCategoryAttribute(string name) : base(name)
        {
        }

        [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
        public class PropertyAttribute : GenerationDecorationAttribute
        {
            public string Name { get; private set; }

            public PropertyAttribute(string name)
            {
                Name = name;
            }
        }
    }
}
