﻿namespace Shygen.Editor.Test.DefaultModule.Models
{
    using Generation.Attributes;
    using Runtime.Attributes;

    [ModelCategory("Player")]
    [ModelCategoryAttribute.Property("Prop1")]
    [ModelCategoryAttribute.Property("Prop2")]
    [ModelCategoryAttribute.Property("Prop3")]
    [ShygenModule("DefaultModule")]
    public partial class PlayerModel
    {
        private void Bla()
        {
            Prop1 = null;
        }
    }
}
