﻿namespace Shygen.Editor.Test.Template.Generation
{
    using Categories;
    using Runtime.Attributes;

    [GroupConfiguration("DefaultGroup", "Shygen.Editor.Test", "Shygen.Editor.Test\\GeneratedCode", "DefaultModule")]
    [TemplateConfiguration(typeof(ModelCategory),
        "Shygen.Editor.Test.Template\\Generation\\Templates\\ModelGenerator.tat.cs")]
    public class GenerationConfiguration
    {
    }
}
