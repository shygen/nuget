﻿using System.Text;

namespace Shygen.Editor.Test.Template.Generation.Templates
{
    using System;
    using Categories;
    using Generators;
    using Runtime.Attributes;

    [TemplateName("Model.ModelGenerator")]
    public class ModelGenerator : CSharpGenerator
    {
        protected override IterationMode Mode => IterationMode.Single;

        protected override void ModifyTemplate(CSharpTemplate template)
        {
            var modelData = (ModelDataDraft)Data;

            template.ReplaceGroup("property", () =>
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < modelData.Properties.Count; i++)
                {
                    var prop = modelData.Properties[i];

                    var group = template.GetGroupInstance("property");

                    group.Replace("PropertyEntry;", match =>
                    {
                        return prop + ";" + Environment.NewLine;
                    });
                    sb.Append(group.Output());
                }
                return sb.ToString();
            });

            template.Replace("GenerationTemplate.UnityTags", match => ModuleNamespace);

            template.Replace("TemplateClass", match => Data.Name);
        }

        protected override string GetFileName()
        {
            return Data.Name + "Model";
        }
    }
}


//%<template provider="Shygen.Editor.Test.Template.Generation.Templates.ModelGenerator">

namespace GenerationTemplate.UnityTags
{
    public partial class TemplateClassModel
    {
        //%<group name="property">
        public object PropertyEntry;
        //%</group>
    }
}

//%</template>