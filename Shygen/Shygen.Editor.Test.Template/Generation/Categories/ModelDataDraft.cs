﻿namespace Shygen.Editor.Test.Template.Generation.Categories
{
    using System;
    using System.Collections.Generic;
    using Runtime.Attributes;
    using Test.Generation.Attributes;

    public class ModelDataDraft : DataDraft
    {
        public List<string> Properties = new List<string>();

        public override void Initialize(Type type)
        {
            base.Initialize(type);

            object[] decorations = type.GetCustomAttributes(typeof(GenerationDecorationAttribute), false);

            for (var i = 0; i < decorations.Length; i++)
            {
                object decoration = decorations[i];

                if (decoration is ModelCategoryAttribute.PropertyAttribute property)
                {
                    Properties.Add(property.Name);
                }
            }
        }
    }
}