﻿namespace Shygen.Editor.Test.Template.Generation.Categories
{
    using System;
    using System.ComponentModel;
    using Attributes;
    using Interfaces;
    using Test.Generation.Attributes;

    [RuntimeAttributeType(typeof(ModelCategoryAttribute))]
    [DataDraftType(typeof(ModelDataDraft))]
    [DisplayName("Models")]
    public class ModelCategory : ICodeGenerationCategory
    {
        public string Name => "Models";
        public string Namespace => Name;
        public Type DataDraftType { get { return typeof(ModelDataDraft); } }
        public Type RuntimeAttributeType { get { return typeof(ModelCategoryAttribute); } }
    }
}